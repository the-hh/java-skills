package com.hh.oop_tiny_tasks.p_02;

public class CustomFor {
    public static void main(String[] args) {
        String[] chars = new String[]{"Thea", "Jan", "Lea"};
        String[] tools = new String[]{"one", "two", "three"};

        for (int j = 0, k = 0; j < 3; j++, k++) {
            System.out.println(chars[j] + " " + tools[k]);
        }
    }
}
