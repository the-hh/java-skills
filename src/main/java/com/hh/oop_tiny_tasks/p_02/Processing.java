package com.hh.oop_tiny_tasks.p_02;

interface AmountProcessing {
    void processAmount(double amount);

    public static final double ADJUST = 10;

}

public class Processing implements AmountProcessing {

    public static void main(String[] args) {
        double amount = 50;
        new Processing().processAmount(++amount);
    }

    @Override
    public void processAmount(double amount) {
        amount += ++amount * ADJUST;
        System.out.println("Amount: " + amount);
    }
}
