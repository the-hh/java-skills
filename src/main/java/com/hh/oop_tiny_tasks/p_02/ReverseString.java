package com.hh.oop_tiny_tasks.p_02;

public class ReverseString {

    public static void main(String[] args) {
        reverse("aub");
    }

    static void reverse(String str) {
        if (str.length() <= 1 || str == null) {
            System.out.println(str);
        } else {
            System.out.println(str.charAt(str.length() - 1));
            reverse(str.substring(0, str.length() - 1));

        }
    }
}
