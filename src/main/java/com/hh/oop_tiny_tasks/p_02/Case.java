package com.hh.oop_tiny_tasks.p_02;

class TransactionException extends Exception {
}

abstract class Fraud {
    protected abstract void isFraud() throws TransactionException;
}

public class Case extends Fraud {
    @Override
    protected void isFraud() throws TransactionException {
        throw new RuntimeException();
    }

    public static void main(String[] args) throws Throwable {
        new Case().isFraud();
    }
}
