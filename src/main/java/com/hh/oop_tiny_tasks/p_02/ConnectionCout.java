package com.hh.oop_tiny_tasks.p_02;

public class ConnectionCout {

    private int connectionCout = 0;

    public int getConnectionCout() {
        return connectionCout++;
    }

    public static void main(String[] args) {
        ConnectionCout conn1 = new ConnectionCout();
        ConnectionCout conn2 = new ConnectionCout();
//        System.out.println(Connection.getConnectionCout());
    }
}
