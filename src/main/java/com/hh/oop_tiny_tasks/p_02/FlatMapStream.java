package com.hh.oop_tiny_tasks.p_02;

import java.util.stream.Stream;

public class FlatMapStream {

    private static Stream<String> getWords(String firstWord, String secondWord) {
        return Stream.of(firstWord, secondWord);
    }

    public static void main(String[] args) {
        String answer = Stream.of(getWords("voyage", "rue"), getWords("habitude", "quel"), getWords("oui", "parfaite"))
                .flatMap(x -> x)
                .filter(x -> !x.isEmpty())
                .max((a, b) -> b.length() - a.length())
                .get();
        System.out.println(answer);


    }
}
