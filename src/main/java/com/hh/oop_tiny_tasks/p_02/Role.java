package com.hh.oop_tiny_tasks.p_02;

class Administrator extends Role {
    public void register() {
        System.out.println("administrator");
    }
}

public class Role {

    public void register() {
        System.out.println("user");
    }

    public static void main(String[] args) {
        Role user = new Administrator();
        Administrator admin = new Administrator();
        user.register();
        admin.register();
    }
}
