package com.hh.oop_tiny_tasks.p_02;

final public class Place {
    final private int population = 250;

    final protected class City {
        private final int population = 100000;

        public int getPopulation() {
            return population;
        }
    }

    final City city = new City();

    public static void main(String[] args) {
        System.out.println(new Place().city.getPopulation());
    }
}
