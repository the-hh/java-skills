package com.hh.oop_tiny_tasks.p_02;

public class Messenger {

    class ClientException extends Exception {
    }

    class ServerException extends Exception {
    }

    class UnknownException extends RuntimeException {
    }

    public void connect() throws Throwable, ClientException, ServerException {
        try {
            throw new IllegalStateException();
        } catch (Exception ex2) {
            System.out.println("Unknown problem");
            throw ex2;
        }
    }

    public static void main(String[] args) throws Throwable {
        try {
            new Messenger().connect();
        } finally {
            System.out.println("Completed");
        }
    }

}
