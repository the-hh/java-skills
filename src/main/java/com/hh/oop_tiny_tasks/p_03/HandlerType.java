package com.hh.oop_tiny_tasks.p_03;


interface Handler<T> {
    void process(T t);
}


class OrderHandler implements Handler<HandlerType> {
    @Override
    public void process(HandlerType handlerType) {
        System.out.println(handlerType);
    }
}

//class ActinityHandler implements Handler<Q022> {
//
//    @Override
//    public void process(T t) {System.out.println(t);}
//}

class BeneficiaryHandler<T> implements Handler<T> {
    @Override
    public void process(T t) {
        System.out.println(t);
    }
}


public class HandlerType {
    public static void main(String[] args) {
        Handler<HandlerType> handler1 = new OrderHandler();
//        Handler<HandlerType> handler2 = new ActinityHandler();
        Handler<HandlerType> handler3 = s -> System.out.println(s);
        Handler<HandlerType> handler4 = new BeneficiaryHandler<>();
    }
}

