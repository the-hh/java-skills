package com.hh.oop_tiny_tasks.p_03;

import java.util.Comparator;
import java.util.stream.Stream;

public class IntegerComparator {
    public static void main(String[] args) {
        Stream<Integer> numbers = Stream.of(4, 9, 5);
        Comparator<Integer> integerComparator = (a, b) -> b - a;
        numbers.sorted(integerComparator).forEach(System.out::println);
    }
}
