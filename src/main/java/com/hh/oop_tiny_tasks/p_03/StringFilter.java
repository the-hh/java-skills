package com.hh.oop_tiny_tasks.p_03;

import java.util.stream.Stream;

public class StringFilter {
    public static void main(String[] args) {
        Stream<String> words = Stream.of("driver", "effect", "cat");
        System.out.println(words.filter(b -> b.startsWith("e")).count());
    }
}
