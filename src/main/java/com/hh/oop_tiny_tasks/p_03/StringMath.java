package com.hh.oop_tiny_tasks.p_03;

public class StringMath {
    public static void main(String[] args) {
        StringMath s = new StringMath();
        s.create(2, 4, "C");
    }

    void create(int a, int b, String phrase) {
        System.out.print(phrase + 6 + 1 + "-");
        System.out.print(a + b);
        System.out.print("-" + a + b + "-");
        System.out.print(processPhrase(phrase) + a + b);
    }

    private String processPhrase(String phrase) {
        return phrase + "Z";
    }
}
