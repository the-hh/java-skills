package com.hh.oop_tiny_tasks.p_03;

import java.util.concurrent.atomic.AtomicBoolean;

public class Q029 {
    private static AtomicBoolean isActive = new AtomicBoolean();
    private Object lock = new Object();

    public synchronized boolean isActiveV1(boolean newValue) {
        return isActive.getAndSet(newValue);
    }

    public synchronized boolean isActiveV2(boolean expectedValue, boolean newValue) {
        return isActive.compareAndSet(expectedValue, newValue);
    }

    public boolean isActiveV3(boolean newValue) {
        synchronized (lock) {
            return isActive.getAndSet(false);
        }
    }
}
