package com.hh.oop_tiny_tasks.p_03;

public class Email {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static void modifyEmail(Email user, String email) {
        user = new Email();
        user.setEmail(email);
    }

    public static void main(String[] args) {
        Email u1 = new Email();
        u1.setEmail("123@test.ee");
        Email u2 = new Email();
        u2.setEmail("www@test.ee");
        modifyEmail(u1, "TTT@post.ee");
        System.out.println(u1.getEmail());
        System.out.println(u2.getEmail());
    }
}
