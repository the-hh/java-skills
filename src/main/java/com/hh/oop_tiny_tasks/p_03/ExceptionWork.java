package com.hh.oop_tiny_tasks.p_03;

import javax.naming.InvalidNameException;
import javax.naming.NamingException;

public class ExceptionWork {

    public static void main(String[] args) {
        new ExceptionWork().bind();
    }

    public void bind() {
        try {
            throw new NamingException("Naming failure");
        } catch (Exception e) {
            try {
                throw new InvalidNameException("Empty name not allowed");
            } catch (NamingException namingException) {
                System.out.println("Bind failed: NamingException");
            } catch (EnumConstantNotPresentException enumConstantNotPresentException) {
                System.out.println("Bind failed: EnumConstantNotPresentException");
            }
        }
    }
}
