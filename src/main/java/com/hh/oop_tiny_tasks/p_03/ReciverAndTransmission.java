package com.hh.oop_tiny_tasks.p_03;

public class ReciverAndTransmission {

    public static void main(String[] args) throws InterruptedException {
        Thread reciver = new Thread() {
            public void run() {
                System.out.print("reciver");
            }
        };

        Thread transmission = new Thread() {
            public void run() {
                System.out.print("transmission");
            }
        };

        reciver.start();
        reciver.sleep(5000);
        transmission.start();
        transmission.sleep(5000);
        System.out.println("main");


    }
}
