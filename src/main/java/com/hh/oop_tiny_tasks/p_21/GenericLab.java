package com.hh.oop_tiny_tasks.p_21;

class Message<T> {
    private T messageText;

    public Message(T messageText) {
        this.messageText = messageText;
    }
}

class ErrorMessage<S extends CharSequence> extends Message<String> {

    public ErrorMessage(String messageText) {
        super(messageText);
    }

    void setErrorMessage(S s) {
        System.out.println("set ErrorMessage");
    }
}

class ErrorMessage2<S extends CharSequence> extends Message<S> {


    public ErrorMessage2(S messageText) {
        super(messageText);
    }

    void setErrorMessage(S s) {
        System.out.println("set ErrorMessage");
    }
}

public class GenericLab {
}
