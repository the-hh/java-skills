package com.hh.oop_tiny_tasks.p_21;


import java.util.PriorityQueue;

public class Order {

    private String one = null;
    private Integer order = null;

    public enum AType {
        ONE, OTHER
    }


    public static void main(String[] args) {

        AType aType = AType.ONE;

        switch (aType) {
            case ONE:
                System.out.println("ONE");
            case OTHER:
                System.out.println("OTHER");
        }
        System.out.println();

        PriorityQueue<String> pq = new PriorityQueue<>();
        pq.add("A456");
        pq.add("A153");
        pq.add("A999");
        String order;
        while ((order = pq.poll()) != null) {
            System.out.println(order);
        }
    }

}
