package com.hh.oop_tiny_tasks.p_21;

import java.net.http.HttpResponse;

public interface Client {
    abstract HttpResponse get(String path) throws ResponseException;
}
