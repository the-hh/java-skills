package com.hh.oop_tiny_tasks.p_11;

public class Singleton {

    /**
     * The Singleton pattern can be used in situations where you want to ensure that only one instance of a class is created and that it can be accessed globally by other classes. It’s useful when you want to control the number of instances of a class that can be created, or when you want to provide a single point of access to a shared resource.
     *
     * Some examples of where you might use the Singleton pattern include:
     *
     * Logging
     * Database connections
     * Caching
     * Thread pools
     */

    private static Singleton instance;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
