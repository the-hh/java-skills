package com.hh.oop_tiny_tasks.p_11;

public class Fibonacci {

    static int method(int n) {
        if (n <= 1) {
            return n;
        } else {
            return method(n - 1) + method(n - 2);
        }
    }

    public static void main(String[] args) {
        int number = 8;
        for (int i = 1; i <= number; i++) {
            System.out.print(method(i) + " ");
        }
    }
}
