package com.hh.oop_tiny_tasks.p_11;

class Applicant {
    private String name;

    public Applicant(String name) {
        this.name = name;
    }

    public Applicant() {
        this("");
        // Q130 will compile because a no-args constructor in Applicant class is presented
    }

    public String getName() {
        return name;
    }
}

public class Participant2 extends Applicant {

    public void process() {
    }

    public void remove() {
    }
}
