package com.hh.oop_tiny_tasks.p_11;

abstract class AgileEvent {
    protected final String eventType;

    AgileEvent(String eventType) {
        this.eventType = eventType;
    }
}

interface Recurring {
}

public class Event extends AgileEvent implements Recurring {
    Event() {
        super("Sprint Planning");
    }

    public AgileEvent get() {
        return this;
    }

    public static void main(String[] args) {
        Recurring event = (Recurring) (AgileEvent) new Event().get();
        System.out.println(((Event) event).eventType);
    }
}
