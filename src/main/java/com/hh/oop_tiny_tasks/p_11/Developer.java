package com.hh.oop_tiny_tasks.p_11;


class Supervisor {
}

class Employee {
}

abstract class JacaDeveloper extends Developer {
    //    public Employee getSupervisor() {
//        return new Employee();
//    }
    public Supervisor getSupervisor() {
        return new Supervisor();
    }


}

abstract public class Developer {
    public Supervisor getSupervisor() {
        return new Supervisor();
    }

    abstract public Supervisor getSupervisor(int level);
}
