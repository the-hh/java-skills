package com.hh.oop_tiny_tasks.p_11;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.function.Predicate;

class Student {
    public double grade;

    public Student(double grade) {
        this.grade = grade;
    }
}

public class StudentGradePredicate {

    public static void main(String[] args) {
        Student student = new Student(2);
        new StudentGradePredicate().admission(s -> s.grade > 3, student);
        new StudentGradePredicate().admission(s -> s.grade >= 2, student);
    }

    private void admission(Predicate<Student> admit, Student student) {
        System.out.print((admit.test(student) ? "Admit" : "Reject") + " ");
    }

    public void writeStrem() throws IOException {
        final Writer w = new BufferedWriter(new FileWriter("file.out"));
        w.write("sdfsdf");
        w.close();
    }

    public void writeToFile() throws IOException {
        try {
            final Writer w = new BufferedWriter(new FileWriter("file.out"));
            w.write("sdfsdf");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
