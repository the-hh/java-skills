package com.hh.oop_tiny_tasks.p_11;

import java.util.function.Predicate;

public class DataFilter {

    public static void main(String[] args) {

        int[] intArray = {-20, -24, 20, 23, 24, 35, 3679, 3680};
        filterData(intArray, num -> {
            boolean isEnum = num % 2 == 0;
            boolean isAbsLenTwoDigits = Integer.toString(Math.abs(num)).length() == 2;
            return isEnum && isAbsLenTwoDigits;
        });

    }

    public static void filterData(int[] data, Predicate<Integer> pred) {
        for (int value : data) {
            if (pred.test(value)) {
                System.out.println(value + " ");
            }
        }
        System.out.println();
    }

}
