package com.hh.oop_tiny_tasks.p_11;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComplexCollection {


    public static void main(String[] args) {
        List<Map<List<String>, List<Double>>> list = new ArrayList<>();
        Map<List<String>, List<Double>> map = new HashMap<>();
        list.add(null);
        list.add(map);
        list.add(new HashMap<List<String>, List<Double>>());
        for (Map entry : list) {
            System.out.print(entry + " ");
        }
    }
}
