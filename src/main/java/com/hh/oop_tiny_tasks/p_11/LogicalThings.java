package com.hh.oop_tiny_tasks.p_11;

public class LogicalThings {

    static boolean portable(int rating) {
        System.out.println("portable");
        return rating > 6 ? true : false;
    }

    static boolean reliable(int rating) {
        System.out.println("reliable");
        return rating > 6 ? true : false;
    }

    static boolean userFriendly(int rating) {
        System.out.println("user-friendly");
        return rating > 6 ? true : false;
    }

    static boolean selfDocumenting(int rating) {
        System.out.println("self-documenting");
        return rating > 6 ? true : false;
    }

    private static int num;

    public static void main(String[] args) {
//        boolean t1 = userFriendly(4) & reliable(7);
//        boolean t2 = t1 ^ selfDocumenting(8);
//        boolean result = portable(5) | t2;
        boolean result = portable(5) | userFriendly(4) & reliable(7) ^ selfDocumenting(8);
        System.out.println(result);

        num++;
        System.out.println(num);

    }
}
