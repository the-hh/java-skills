package com.hh.oop_tiny_tasks.p_11;

import java.util.OptionalDouble;
import java.util.stream.Stream;

class Participant {
    private double score;

    public Participant(double score) {
        this.score = score;
    }

    public double getScore() {
        return score;
    }
}

public class PersonScore {

    public static void main(String[] args) {
        Stream<Participant> students = Stream.of(
                new Participant(92.5),
                new Participant(83.5),
                new Participant(91));
        OptionalDouble opt = students
                .mapToDouble(Participant::getScore)
                .peek(System.out::println)
                .max();
    }
}
