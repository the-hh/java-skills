package com.hh.oop_tiny_tasks.p_04;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorServiceDemo {
    public void downoadFile() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Future result = executorService.submit(() -> System.out.println(""));
        executorService.shutdown();
//        System.out.println(result.get());
    }

    public static void main(String[] args) {
        try {
            new ExecutorServiceDemo().downoadFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
