package com.hh.oop_tiny_tasks.p_04;

import java.util.Optional;

public class EmptyOptional {
    public static void main(String[] args) {
        scheduleEvent(Optional.empty());
    }

    private static void scheduleEvent(Optional<Object> event) {
        System.out.println(event.orElse("No event"));
    }
}
