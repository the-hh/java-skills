package com.hh.oop_tiny_tasks.p_01;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class LanguageSet {
    public static void main(String[] args) {
        List<String> languagelist = new ArrayList<>();
        languagelist.add("Java");
        languagelist.add("Scala");
        languagelist.add("Java");
        Set<String> languageSet = new TreeSet<>(languagelist);
        System.out.print(languageSet.size());
        System.out.print(" - ");
        System.out.print(languageSet.iterator().next());
    }

}
