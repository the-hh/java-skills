package com.hh.oop_tiny_tasks.p_01;

import java.util.ArrayList;
import java.util.List;

class Player {
    private String userName;

    public Player(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return getUserName();
    }
}

public class PlayerStream {

    public static void main(String[] args) {
        final List<Player> players = new ArrayList<>();
        players.add(new Player("Grumphic"));
        players.add(new Player("Dorat"));
        players.add(new Player("Biotone"));
        players.removeIf(s -> s.equals("Grumphic"));
        System.out.println(players);

    }
}
