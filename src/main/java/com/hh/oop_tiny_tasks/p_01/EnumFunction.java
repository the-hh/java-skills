package com.hh.oop_tiny_tasks.p_01;

public class EnumFunction {

    public enum When {ALWAYS, UNKNOWN, MABY, NEVER}

    public static void main(String[] args) {
        System.out.print(When.ALWAYS.ordinal());
        System.out.print("," + When.valueOf("NEVER").name());
    }

}
