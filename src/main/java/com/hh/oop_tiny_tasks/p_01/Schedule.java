package com.hh.oop_tiny_tasks.p_01;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Schedule {

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);

    public void clearData() {
        scheduledExecutorService.scheduleAtFixedRate(() -> System.out.println("Delete site data"), 10, 10, TimeUnit.HOURS);
        // scheduledExecutorService.scheduleWithFixedDelay(() -> System.out.println("Delete Cookies"), 1, TimeUnit.DAYS);
        scheduledExecutorService.execute(() -> System.out.println("Delete chased web contnt"));
    }
}
