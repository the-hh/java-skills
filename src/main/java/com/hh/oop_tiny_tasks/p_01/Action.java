package com.hh.oop_tiny_tasks.p_01;

class BaseAction {
}

public class Action extends BaseAction {
    public static void main(String[] args) {
        BaseAction baseAction = new BaseAction();
        execute(baseAction);
    }

    public static void execute(BaseAction baseAction) {
        if (baseAction instanceof Action) {
            System.out.println("it is Q006_Action");
        } else if (baseAction instanceof BaseAction) {
            System.out.println("it is BaseAction");
        }
    }
}
