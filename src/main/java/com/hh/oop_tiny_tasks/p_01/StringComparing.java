package com.hh.oop_tiny_tasks.p_01;
public class StringComparing {
    public static void main(String[] args) {
        String message = " Good Morning ";
        String newMessage = message.replaceAll("[A-Z]*", "").trim();
        int endIndex1 = message.trim().indexOf(" ") + 2;
        String sub1 = message.substring(2, endIndex1);
        int beginIndex2 = message.trim().indexOf(" ") + 3;
        String sub2 = message.substring(beginIndex2, message.length() - 1);
        String sub = sub1.concat(sub2);
        System.out.println(newMessage.equals(sub));
    }
}
